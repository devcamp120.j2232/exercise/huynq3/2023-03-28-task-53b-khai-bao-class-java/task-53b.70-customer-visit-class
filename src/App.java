import com.dev.Customer;
import com.dev.Visit;
import java.util.Date;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Customer customer1= new Customer("Huy", true, "Gold");
        Customer customer2= new Customer("Yến", false, "Diamond");
        System.out.println(customer1);
        System.out.println(customer2);

        Visit visit1= new Visit(customer1, "28/03/2023", 1000000, 2000000);
        Visit visit2= new Visit(customer2, "28/03/2023", 1500000, 2500000);
        System.out.println(visit1);
        System.out.println(visit1);
        System.out.println("Tổng chí phí chuyến 1: = "+visit1.getTotalExpense());
        System.out.println("Tổng chí phí chuyến 2: = "+visit2.getTotalExpense());
    }
}
